import { Component } from '@angular/core';
import { CartService } from '../services/cart.service';
import { InapppurchaseService } from '../services/inapppurchase.service';

const products = require('./../data/products.json');

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  products = products;
  constructor(public cartService: CartService, public iapService: InapppurchaseService) {

  }


  addToCart(item){
    console.log(item);
    this.cartService.addToCart(item);

    this.iapService.checkout(item.product_id);

  }


}
