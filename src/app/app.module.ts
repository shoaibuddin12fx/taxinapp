import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { SqliteService } from './services/sqlite.service';
import { StorageService } from './services/storage.service';
import { CartService } from './services/cart.service';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { InapppurchaseService } from './services/inapppurchase.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, SQLite,
    InAppPurchase2,
    InapppurchaseService,
    SqliteService,
    StorageService,
    CartService],
  bootstrap: [AppComponent],
})
export class AppModule { }
