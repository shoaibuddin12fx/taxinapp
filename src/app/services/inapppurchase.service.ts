import { Injectable } from '@angular/core';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { Platform } from '@ionic/angular';

const products = require('./../data/products.json');

@Injectable({
  providedIn: 'root'
})
export class InapppurchaseService {

  iapProducts = [];
  constructor(public platform: Platform, private iap2: InAppPurchase2) {
    // this.products = products;
  }

  // register your product on app start for easy verification
  async setup(){

    await this.platform.ready();
    this.iap2.verbosity = this.iap2.DEBUG;

    products.forEach( product => {
      this.registerProduct(product);
    });

    this.iap2.refresh();

  }

  registerProduct(product){
    this.iap2.register({
      id: product.product_id,
      type: this.iap2.CONSUMABLE
    });
    const res = this.iap2.get(product.product_id);
    this.iapProducts.push(res);
    this.registerHandlersForPurchase(product.product_id);
    // restore purchase


  }

  registerHandlersForPurchase(productId) {
    let self = this.iap2;
    this.iap2.when(productId).updated( (product) => {
      if (product.loaded && product.valid && product.state === self.APPROVED && product.transaction != null) {
        product.finish();
      }
    });
    this.iap2.when(productId).registered((product: IAPProduct) => {
      alert(` owned ${product.owned}`);
    });
    this.iap2.when(productId).owned((product: IAPProduct) => {
      alert(` owned ${product.owned}`);
      product.finish();
    });
    this.iap2.when(productId).approved((product: IAPProduct) => {
      alert('approved');
      product.finish();
    });
    this.iap2.when(productId).refunded((product: IAPProduct) => {
      alert('refunded');
    });
    this.iap2.when(productId).expired((product: IAPProduct) => {
      alert('expired');
    });
  }

  checkout(productId) {
    // this.registerHandlersForPurchase(productId);

    try {

      const product = this.iap2.get(productId);
      console.log('Product Info: ' + JSON.stringify(product));

      this.iap2.order(productId).then((p) => {
        console.log('Purchase Succesful' + JSON.stringify(p));
      }).catch((e) => {
        console.log('Error Ordering From Store' + e);
      });

    } catch (err) {
      console.log('Error Ordering ' + JSON.stringify(err));
    }
  }




}
