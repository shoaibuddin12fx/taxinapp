import { Injectable } from '@angular/core';
import { SQLite, SQLiteDatabaseConfig, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { browserDBInstance } from './browser-db-instance.service'
import { StorageService } from './storage.service';

declare var window: any;
const SQL_DB_NAME = '__taxinfile.db';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  db: any;
  config: SQLiteDatabaseConfig = {
    name: '__taxinfile.db',
    location: 'default'
  }
  
  public msg = "Sync In Progress ...";

  constructor(
    private storage: StorageService, 
    private platform: Platform, 
    private sqlite: SQLite, 
    ) {

      


  }

  public initialize() {

    return new Promise(resolve => {
      this.storage.getKey('is_database_initialized').then(async v => {
        if (!v) {
          await this.initializeDatabase();
          resolve(true);
        } else {
          resolve(true);
        }
      })
    })

  }

  async initializeDatabase() {

    return new Promise(async resolve => {
      await this.platform.ready();
      // initialize database object
      await this.createDatabase()
      
      // initialize all tables

      // initialize users table
      await this.initializeCartTable();
      // initialize the user flags table for screens
      
      this.storage.setKey('is_database_initialized', true);
      resolve(true);
    })


  }

  async createDatabase(){
    return new Promise( async resolve => {
      if(this.platform.is('cordova')){
        await this.sqlite.create(this.config).then(db => {
          this.msg = 'Database initialized';
          this.db = db
        });
      }else{
        let _db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
        this.db = browserDBInstance(_db);
        this.msg = 'Database initialized';
        
      }
      resolve(true);
    })
    

  }

  async initializeCartTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS cart(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "title TEXT, " 
      sql += "image TEXT, " 
      sql += "price INTEGER DEFAULT 0 "       
      sql += ")";

      this.msg = 'Initializing Cart ...';
      resolve(this.execute(sql, []));
    })

  }

  public async setCartItemInDatabase(item) {
    return new Promise(async resolve => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      var sql = "INSERT OR REPLACE INTO cart(";
      sql += "id, ";
      sql += "title, " 
      sql += "image, " 
      sql += "price " 
      sql += ") ";

      sql += "VALUES (";

      sql += "?, "
      sql += "?, " 
      sql += "?, " 
      sql += "? "       
      sql += ")";

      var values = [
        item.id,
        item.title,
        item.image,
        item.price,
      ];

      await this.execute(sql, values);

      resolve(await this.getFullCart());

    })
  }


  public async getFullCart(){

    return new Promise(async resolve => {
      let sql = "SELECT * FROM cart";
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }

    })
  }

  public async deleteCartItem(itemId){

    return new Promise(async resolve => {
      let sql = "DELETE FROM cart where id = ?";
      let values = [itemId];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      resolve(await this.getFullCart());

    })
  }

  public async deleteFullCart(){

    return new Promise(async resolve => {
      let sql = "DELETE FROM cart";
      let values = [];

      let d = await this.execute(sql, values);
      resolve(await this.getFullCart());

    })
  }

  execute(sql, params) {
    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase()
      }

      this.db.executeSql(sql, params).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      })
      
    })
  }


  private setValue(k, v) {
    return new Promise(resolve => {
      this.storage.setKey(k, v).then(() => {
        resolve({ k: v });
      });
    })

  }

  private getValue(k): Promise<any> {
    return new Promise(resolve => {
      this.storage.getKey(k).then(r => {
        resolve(r);
      });
    })

  }

  private getRows(data) {
    var items = []
    for (let i = 0; i < data.rows.length; i++) {
      let item = data.rows.item(i);

      items.push(item);
    }

    return items;
  }




}
