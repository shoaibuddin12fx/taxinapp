import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(public sqlite: SqliteService) { }

  getAllCart(){
    return this.sqlite.getFullCart();
  }

  addToCart(item){
    return this.sqlite.setCartItemInDatabase(item);
  }

  removeItemFromCart(itemId){
    return this.sqlite.deleteCartItem(itemId)
  }

}
