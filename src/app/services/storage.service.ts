import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(public platform: Platform) { 

  }

  setKey(key, value): Promise<any>{

    return new Promise(resolve => {      
      resolve(localStorage.setItem(key, JSON.stringify(value)))  
    })

  }

  getKey(key): Promise<any>{
    
    return new Promise(resolve => {
      let v = JSON.parse(localStorage.getItem(key));
      console.log(v);
        if (typeof v === 'string' || v instanceof String){
          v = v.replace(/['"]+/g, '');
        }
        resolve(v)
    })

  } 

  setUserToken(token){
    this.setKey('token', token);
  }  

  getUserToken(){
    return this.getKey('token');
  }

  setUserData(data){
    this.setKey('userdata', data);
    this.setUserToken(data['token']);
  }  


}
