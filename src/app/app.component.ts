import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { InapppurchaseService } from './services/inapppurchase.service';
import { SqliteService } from './services/sqlite.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor( private platform: Platform, public sqlite: SqliteService, public iapService: InapppurchaseService) {
    this.initialize();
  }

  initialize(){
    this.platform.ready().then( async () => {
      await this.sqlite.initializeDatabase();
      this.iapService.setup();
    });
  }
}
