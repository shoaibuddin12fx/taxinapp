import { Component } from '@angular/core';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  items = [];
  total = 0;

  constructor(public cartService: CartService) {}

  ionViewWillEnter(){
    console.log("revisit")
    this.initialize()
  }

  async initialize(){
    this.items = await this.cartService.getAllCart() as [];
    this.total = this.items.reduce( function(a, b){
      return a + b['price'];
  }, 0)
  }

  async removeItem(item){
    await this.cartService.removeItemFromCart(item.id)
    this.initialize();

  }

}
